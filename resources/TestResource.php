<?php 
	/**
	* Clase para retornar las locaciones de carros
	*/
	class TestResource extends restResourseAbstract
	{
		/**
		 * Metodo que devuelve los parametros con que fue invocado
		 * @return [arrya [arreglo con parametros de invocacion]
		 */
		public function getAction(){
			
			$params = RestUtils::getParams();

			return $params;
		}

		public function postAction(){

		}
		
		public function putAction(){

		}
		
		public function deleteAction(){

		}
		
	}
