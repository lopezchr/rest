# README #

### Descripcion ###

Este proyecto esta enfocado a brindar un marco para aplicaciones REST mediente una estructura que permite facilitar el proceso de desarrollo. 

Las peticiones que se realizan a el sercicio son de la siguiente forma :

** /url/to/my/directory/host/..../rest/nombre_recurso/variable1/valor_variable_1/variable2/valor_variable_2 **

El microframework esta inspirado en Zend Framework en la menera en la que se realizan las peticiones incluyendo un autoloader para los recursos REST.

Las respuestas del servicio son de tipo json de la forma:

** {"success":true,"description":'mensaje'} **
** {"success":true,"description":123333} **
** {"success":true,"description":{dato1:'asdd',dato2:15345}} **
** {"success":false,"description":"mensaje"} **

### Instalacion ###

Se requiere :
* apache2.
* configuracion de servidor: AllowOverride para que el .htaccess funcione adecuadamente.


### Modo de Uso ###

Para usar el microframework hay que copiarlo en el directorio host y crear los recursos en la carpeta de recursos con nombre {Mirecurso}Resource.php. Estos son clases que deben extender de restResourseAbstract y deben tener exactamente el mismo nombre que el nombre de su archivo. Este sera accesible desde /url/to/my/directory/host/..../rest/{mirecurso}. Notese que la primera letra de el archivo es en mayuscula pero en url preferiblemente en minuscula.

Los recursos, están constituidos por 4 metodos correspondeintes a cada verbo de las peticiones http:

getAction, postAction, putAction y deleteAction.

```
#!php

<?php 
	/**
	* Clase para retornar las locaciones de carros
	*/
	class TestResource extends restResourseAbstract
	{
		/**
		 * Metodo que devuelve los parametros con que fue invocado
		 * @return [arrya [arreglo con parametros de invocacion]
		 */
		public function getAction(){
			
			$params = RestUtils::getParams();

			return $params;
		}

		public function postAction(){

		}
		
		public function putAction(){

		}
		
		public function deleteAction(){

		}
		
	}


```

Estos métodos debe ser modificados y deben retornar la información que se requiera en forma de arreglo, texto u objeto.

Para extraer los parámetros de una petición:

```
#!php

RestUtils::getParams(); //retorna todos los parametros
RestUtils::getParam($key); //retorna un parametro

```

Para retornar un mensaje de error se debe lanzar la excepcion con el clasico:

```
#!php

throw new Exception('mensaje',1);

```

### Acerca de ###

Versión: 1.0.1
Autor: Christian Esteban Lopez Santos
Pais: Colombia 
Email: lopezchr@gmail.com
