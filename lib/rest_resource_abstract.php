<?php 

/**
* Clase abstracta de recursos
*/
abstract class restResourseAbstract
{

	/**
	 * Metodo que ejecuta la accion conrrespondiente y responde un json
	 * @return [type] [description]
	 */
	public function runRest()
	{
		switch(RestUtils::getMethod()){
			case 'POST':
				$response = $this->postAction();
				break;
			case 'GET':
				$response = $this->getAction();
				break;
			case 'PUT':
				$response = $this->putAction();
				break;
			case 'DELETE':
				$response = $this->deleteAction();
				break;
		}

		if ($response === null) {
			throw new Exception("El metodo no esta definido.", 1);
			
		}

		echo json_encode(array("success" => true,"description" => $response));
		
	}

	/**
	 * Metodo que se ejecuta cuando la peticion es GET
	 * @return [array] [arreglo de respuesta]
	 */
	abstract function getAction();

	/**
	 * Metodo que se ejecuta cuando la peticion es POST
	 * @return [array] [arreglo de respuesta]
	 */
	abstract function postAction();

	/**
	 * Metodo que se ejecuta cuando la peticion es PUT
	 * @return [array] [arreglo de respuesta]
	 */
	abstract function putAction();

	/**
	 * Metodo que se ejecuta cuando la peticion es DELETE
	 * @return [array] [arreglo de respuesta]
	 */
	abstract function deleteAction();

}
 ?>