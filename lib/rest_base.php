<?php 

	require_once 'lib/rest_utils.php';
	require_once 'lib/rest_autoloader.php';
	require_once "lib/rest_resource_abstract.php";

	/**
	* Clase base
	*/
	class RestBase
	{
		
		function __construct()
		{
			# code...
		}

		public function run()
		{
			$autoloader = new restAutoloader();
			$autoloader->run();
		}
	}
