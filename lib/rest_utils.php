<?php 

	/**
	* Clase para el manejo de request de REST
	*/
	class RestUtils
	{
		static function getMethod()
		{
			return $_SERVER["REQUEST_METHOD"];
		}

		static function getResourceName()
		{
			$uri = $_SERVER["REQUEST_URI"];
			$uriParts = explode("/", $uri);

			$arrBaseName = explode("-", $uriParts[2]);

			$baseName = '';
			foreach ($arrBaseName as $nameComponent) {
				$baseName .= ucwords($nameComponent);
			}

			return $baseName;
		}

		static function getParams()
		{
			$uri = $_SERVER["REQUEST_URI"];
			$uriParts = explode("/", $uri);

			$arrParams = array_chunk(array_slice($uriParts, 3), 2);

			$arrResult = array();
			foreach ($arrParams as $key => $param) {
				$arrResult[$param[0]] = $param[1];	
			}

			return $arrResult;
		}

		static function getParam($key)
		{
			$params = self::getParams();
			return $params[$key];
		}
	}
