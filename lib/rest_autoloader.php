<?php 

/**
* 
*/
class restAutoloader
{
	public function run()
	{
		try {
			
			$resourceName = RestUtils::getResourceName()."Resource";
			$filename = RESOURCE_PATH."/".$resourceName.".php";

			if(is_file($filename)){
		    	require_once $filename;
				$resource = new $resourceName();
				$resource->runRest();
			}else{
				throw new Exception("Recurso no encontrado", 1);
			}			

		} catch (Exception $e) {
			echo json_encode(array("success"=>false,"description"=>$e->getMessage()));
		}
	}

}

	
